﻿using UnityEngine;
using System.Collections;

public class SoundController : MonoBehaviour {
	public static SoundController Instance;
	public AudioSource soundEffect;
	public AudioSource music;
	public AudioSource Gameover;

	private float lowPitch = 0.95f;
	private float highPitch = 1.05f;

	void Awake () {
		if (Instance != null && Instance != this) {
			
			DestroyImmediate (gameObject);
			return;
		}

		Instance = this;
			DontDestroyOnLoad(gameObject);
	}

	public void PlaySingle(params AudioClip[] clips){
		RandomizeSoundEffect (clips);	
		soundEffect.Play ();
	}

	private void RandomizeSoundEffect(AudioClip[] clips){
		int randomSoundIndex = Random.Range (0, clips.Length);
		float randomPitch = Random.Range (lowPitch, highPitch);

		soundEffect.pitch = randomPitch;
		soundEffect.clip = clips [randomSoundIndex];
	}

}
