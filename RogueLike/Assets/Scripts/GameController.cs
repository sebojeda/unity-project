﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class GameController : MonoBehaviour {

	public static GameController Instance;
	public bool isPlayerTrun;
	public bool areEnemiesMoving;
	public int PlayerCurrentHealth = 50;
	public AudioClip Gameover;

	private BoardController boardController;
	private List<Enemy> enemies;
	private GameObject levelImage;
	private Text LevelText;
	private bool settingUpGame;
	private int secsUntilLevelStart = 2;
	private int currentLevel = 1;

	void Awake () {
		if(Instance != null && Instance != this){
			DestroyImmediate(gameObject);
			return;
		}

		Instance = this;
		DontDestroyOnLoad (gameObject);
		boardController = GetComponent<BoardController> ();
		enemies = new List<Enemy> (); 
	}

	void Start(){
		InitializeGame ();
	}

	private void InitializeGame(){
		settingUpGame = true;
		levelImage = GameObject.Find ("Level Image");
		LevelText = GameObject.Find ("Level Text").GetComponent<Text>();
		LevelText.text = "Day" + currentLevel;
		levelImage.SetActive (true);
		enemies.Clear ();
		boardController.SetupLevel (currentLevel);
		Invoke("DisableLevelImage", secsUntilLevelStart);
	}

	private void DisableLevelImage(){
		
		levelImage.SetActive (false);
		settingUpGame = false;
		isPlayerTrun = true;
		areEnemiesMoving = false;
	}

	private void OnLevelWasLoaded(int levelLoaded){
		currentLevel++;

		InitializeGame ();
	}

	void Update () {
		if (isPlayerTrun || areEnemiesMoving || settingUpGame) {
			return;		
		}

		StartCoroutine (MoveEnemies());
	}

	private IEnumerator MoveEnemies(){

		areEnemiesMoving = true;

		yield return new WaitForSeconds (0.2f);

		foreach (Enemy enemy in enemies) {
			enemy.MoveEnemy();
			yield return new WaitForSeconds(enemy.moveTime);
		}
	
		areEnemiesMoving = false;
		isPlayerTrun = true;

	}

	public void AddEnemyTolist (Enemy enemy){
		enemies.Add (enemy);
	}

	public void GameOver(){
		isPlayerTrun = false;
		SoundController.Instance.music.Stop();
		SoundController.Instance.PlaySingle (Gameover);
		LevelText.text = "You died after " + currentLevel + " days...";
		levelImage.SetActive (true);
		enabled = false;
	
	}

}
